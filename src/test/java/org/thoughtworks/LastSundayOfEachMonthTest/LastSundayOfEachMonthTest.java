package org.thoughtworks.LastSundayOfEachMonthTest;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class LastSundayOfEachMonthTest {
    @Test
    void shouldReturnArrayOfLastSundaysOfEachMonthWhenYearIsGiven() {
        LastSundayOfEachMonth year = new LastSundayOfEachMonth();

        int[] actualArray = year.findLastSundayOfEachMonth(2022);
        int[] expectedArray = {30, 27, 27, 24, 29, 26, 31, 28, 25, 30, 27, 25};
        assertArrayEquals(expectedArray, actualArray);
    }

    @Test
    void shouldThrowExceptionWhenFourDigitsAreNotGiven() {
        LastSundayOfEachMonth year = new LastSundayOfEachMonth();
        assertThrows(ArithmeticException.class, () -> year.findLastSundayOfEachMonth(202));
    }

}
