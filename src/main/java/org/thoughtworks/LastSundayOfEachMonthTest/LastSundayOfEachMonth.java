package org.thoughtworks.LastSundayOfEachMonthTest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LastSundayOfEachMonth {

    public int[] findLastSundayOfEachMonth(int year) {
        String lenOfYear = Integer.toString(year);
        int[] lastSundayDatesArray = new int[12];
        if (lenOfYear.length() != 4)
            throw new ArithmeticException("digits present in year should be four");
        else {
            try {
                String date;
                int numberOfDaysInMonth;
                for (int month = 1; month <= 12; month++) {
                    if (month == 2)
                        numberOfDaysInMonth = 27;
                    else if (month == 8)
                        numberOfDaysInMonth = 31;
                    else if (month % 2 == 1)
                        numberOfDaysInMonth = 31;
                    else
                        numberOfDaysInMonth = 30;

                    for (int i = 0; i < 7; i++) {
                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", java.util.Locale.ENGLISH);
                        date = numberOfDaysInMonth + "/" + month + "/" + year;
                        Date dateObject = sdf.parse(date);
                        sdf.applyPattern("EEE");
                        String str = sdf.format(dateObject);
                        if (str.equals("Sun")) {
                            lastSundayDatesArray[month - 1] = numberOfDaysInMonth;

                            break;
                        }
                        numberOfDaysInMonth--;

                    }
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return lastSundayDatesArray;
    }
}





